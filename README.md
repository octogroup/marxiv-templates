# MarXiv's Request Templates #

This is a collection of the email templates used to request research papers (i.e. preprints and postprints of published/copyrighted "versions of record") from their respective authors.

Templates are generally organized by publisher/journal (e.g. _elsevier_, _nature_, etc.). Papers available "Grey Open Access" follow the _free_ templates. Papers where there is no embargo on the postprint follow the _no-embargo_ templates.

Requests sent to authors who have already shared a paper successfully in MarXiv follow the _success_ templates.

Finally, if there is not a more specific template available, the _general_ template it used.

The templates provided are versioned, and are provided as both plain-text and HTML. For example: _marxiv-free-request-1.html_ and _marxiv-free-request-1.txt_ are HTML and plain-text versions of the same email request. _marxiv-free-request-2_ is the latest version of that template.

## Early templates ##
The first templates we used for the MarXiv Project are in the _originals_ folder. We quickly found that authors were largely ignoring requests to consult their CTAs, so we regularly received copyrighted versions of papers that authors could not publicly share, legally. As such, we started providing specific details of self-archiving policies for various publishers.

## Follow-up emails ##
Each category of request emails (i.e. for the publisher, _wiley_) include the original request email (_marxiv-wiley-request-1_) as well as templates for the first follow-up email (_marxiv-wiley-request-followup-1_) and the second follow-up (_marxiv-wiley-request-second-followup-1_).

Generally speaking, if the author did not respond to the first request, a follow-up was sent after 3 weeks. If the author still did not reply to this first follow-up email, a second follow-up was sent after another 3 week period. We found waiting 3 weeks allowed for most authors to return from travel or other obligations that would keep them from replying in a timely manner.

## Notes on sending ##
We use an email delivery service, SparkPost, to send our messages. With SparkPost's API, we can save these messages as native SparkPost templates and pass-in substitution data to replace the {{patterns}} you see within. If you plan on sending out your own requests, you'll first want to check with your current email service provider to see if there are any limits on your sending. Depending on your Gmail setup, for example, you may be limited to sending 100 messages per day, which can quickly add up! 

## More information on MarXiv ##
MarXiv (rhymes with "archive") is the research repository for the ocean and marine-climate sciences. The repository is located at [https://marxiv.org](https://marxiv.org) while documentation for the project, including submission information, is available at [https://www.marxivinfo.org](https://www.marxivinfo.org). 

MarXiv is a project of [OCTO | Open Communcations for the Ocean](https://www.octogroup.org).