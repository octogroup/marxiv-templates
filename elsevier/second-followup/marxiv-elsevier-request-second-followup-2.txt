Hi {{salutation}},

It's been a few weeks since I last reached out to you, so I just wanted to quickly check back in again and see if I could be of any help. If you're too busy to share your paper yourself, please feel free to just send the manuscript to me and I can take care of it.

If you have any questions or concerns, please let me know.

Thank you,
-Nick

Nick Wehner
Director of Open Initiatives
OCTO | Open Communications for The Ocean
https://www.octogroup.org | https://www.openchannels.org | https://marxiv.org
nick@octogroup.org | nwehner@protonmail.com
Office: +1-206-420-1175
Mobile: +1-206-745-2138
Spokane, Washington, USA (UTC-7)
Learn about the importance of making your research freely-available with MarXiv from Oceans Deeply: https://oct.to/marxivod

>Hi {{salutation}},
>
>I wanted to quickly follow-up with you and see if you had any questions. If there's anything I can do to help, please let me know.
>
>Cheers,
>-Nick
>
>>Hi {{salutation}},
>>
>>On behalf of OCTO, the David and Lucile Packard Foundation, and the Center for Open Science, I’m writing regarding your paper, {{paper}}.
>>
>>I ask that you please share your submitted manuscript (colloquially known as the ‘preprint’) in MarXiv (https://marxiv.org/), the free repository for marine conservation science. MarXiv serves more than 80,000 ocean managers and conservationists worldwide, many of whom would benefit from your research. Your research currently costs {{price}} for the published version, which puts it outside the means of many practitioners, particularly in developing nations.
>>
>>Elsevier actively encourages its authors to share their submitted manuscripts (preprints) in free repositories so the research is available to those who need it. You can read more about the sharing policies for Elsevier at https://www.elsevier.com/about/policies/sharing, where MarXiv is now treated under the same rules as arXiv and RePEc. You can see an example preprint in MarXiv at https://marxiv.org/nfbs3. To be clear, since Elsevier holds the copyright of your paper, you cannot publicly share the final version of your article. However, you are permitted to share either your "submitted/original manuscript" or your "accepted manuscript" immediately (https://www.marxivinfo.org/policies). Elsevier mandates that "accepted manuscripts" be licensed under a CC-BY-NC-ND license (https://creativecommons.org/share-your-work/public-domain/freeworks) which restricts your work from being translated into other languages, so I encourage you to share your "original manuscript" instead, as it may be shared more openly.
>>
>>MarXiv recently launched in November 2017. The average pay-walled journal article in MarXiv is downloaded more than 80 times, saving each reader $30-60. To date, MarXiv has saved ocean conservationists over {{savings}} in per-article download fees.
>>
>>Research shared in repositories like MarXiv (often called "Green Open Access" publications) are cited 30% more on average as more people are able to read your work for free (https://peerj.com/articles/4375/). According to the OECD's analysis, Green Open Access publications are not only cited more than native "Gold Open Access" papers, but Green Open Access papers are also cited more often in law and policy which is critical for ocean conservation (https://www.oecd-ilibrary.org/science-and-technology/drivers-and-implications-of-scientific-open-access-publishing_5jlr2z70k0bx-en).
>>
>>MarXiv is supported by the Packard Foundation, hosted by the Center for Open Science at the University of Virginia, and produced by the nonprofit, OCTO.
>>
>>It only takes 3 minutes to share your research in MarXiv. Simply visit the MarXiv repository at https://marxiv.org and click the “Add a paper” button to share the PDF of your original manuscript. If you have any questions or need help, please do not hesitate to ask me.
>>
>>Thank you for your support,
>>-Nick
